# PolyJSON

## Description

PolyJSON allows to keep multiple similar JSON files as one JSON with additional diff syntax.

For example let's assume that you have two JSON files with `production` and `development` configuration.
If the only difference between both configurations is the `host`, you could keep them in one JSON:

Production config:

```json
{
  "host": "example.org"
  // rest of the config
}
```

Development config:

```json
{
  "host": "localhost"
  // rest of the config
}
```

Single config file:

```json
{
  "host__prod": "example.org",
  "host__dev": "localhost"
  // rest of the config
}
```

To retrieve specific version (`prod` or `dev`) just use the `retrieveJson` function:

```javascript
const config = retrieveJson(oneConfig, "dev");
```

## Usage

`retireveJson` function traverses through `inputJson` and strip the values that does not match to the given `versionName`.
All diff syntax will be removed in the process.

```javascript
const outputJson = retrieveJson(inputJson, versionName);
```

### Property

Store multiple versions of the same property:

```json
{
    "<propertyName>__<version1>": "value1",
    "<propertyName>__<version2>": "value2",
    // ...
    "<propertyName>__<versionN>": "valueN"
}
```

For example:

```javascript
const input = {
    "color__fastCar": "red",
    "color__slowCar": "blue",
}

const output = retrieveJson(input, "fastCar");

// output:
// {
//   "color": "red"
// }
}
```

Property can also be another JSON object or array:

```json
{
  "car__fast": {
    "color": "red",
    "spoiler": true
  },
  "car__slow": {
    "color": "blue",
    "spoiler": false
  }
}
```

PolyJSON properties may also be nested into other objects or arrays:

```json
{
  "cars": [
    {
      "name": "my car",
      "car__fast": {
        "color": "red",
        "spoiler": true
      },
      "car__slow": {
        "color": "blue",
        "spoiler": false
      }
    }
  ]
}
```

### Array object inclusion

PolyJSON supports conditional inclusion of the objects in array:

```json
{
  "cars": [
    {
      "__include": "fast",
      "color": "red",
      "spoiler": "true"
    },
    {
      "__include": "slow",
      "color": "blue",
      "spoiler": false
    },
    {
      "color": "yellow",
      "spoiler": false,
    }
  ]
}
```

Object will be included only if the `__include` value matches the given `versionName`.