import { retrieveJson } from "../polyjson";

test("should return the same json when no control prefix", () => {
  const input = {
    key: "value",
  }

  const output = retrieveJson(input, "some");

  expect(output).toStrictEqual(input);
});


test("input json should not be modified", () => {
  const input = {
    key: {
      otherKey__version1: "value1",
      otherKey__version2: "value2",
    }
  }

  retrieveJson(input, "version1");
  
  expect(input).toStrictEqual({
    key: {
      otherKey__version1: "value1",
      otherKey__version2: "value2",
    }
  });
});

test("should select one version", () => {
  const input = {
    key__version1: "value1",
    key__version2: "value2"
  }

  const output = retrieveJson(input, "version1");

  expect(output).toStrictEqual({
    key: "value1",
  });
});

test("should include array element", () => {
  const input = [
    {
      firstKey: "firstValue",
    },
    {
      __include: "version1",
      midddleKey: "middleValue",
    },
    {
      __include: "version2",
      otherKey: "otherValue",
    },
    {
      lastKey: "lastValue",
    }
  ]

  const output = retrieveJson(input, "version1");

  expect(output).toStrictEqual([
    {
      firstKey: "firstValue",
    },
    {
      midddleKey: "middleValue",
    },
    {
      lastKey: "lastValue",
    }
  ]);
});

test("should include array element when match one of the option", () => {
  const input = [
    {
      firstKey: "firstValue",
    },
    {
      __include: ["version1", "version2"],
      midddleKey: "middleValue",
    },
    {
      __include: "version3",
      otherKey: "otherValue",
    },
    {
      lastKey: "lastValue",
    }
  ]

  const output = retrieveJson(input, "version2");

  expect(output).toStrictEqual([
    {
      firstKey: "firstValue",
    },
    {
      midddleKey: "middleValue",
    },
    {
      lastKey: "lastValue",
    }
  ]);
});

test("should ignore array element when no option match", () => {
  const input = [
    {
      firstKey: "firstValue",
    },
    {
      __include: ["version1", "version2"],
      midddleKey: "middleValue",
    },
    {
      __include: "version3",
      otherKey: "otherValue",
    },
    {
      lastKey: "lastValue",
    }
  ]

  const output = retrieveJson(input, "version3");

  expect(output).toStrictEqual([
    {
      firstKey: "firstValue",
    },
    {
      otherKey: "otherValue",
    },
    {
      lastKey: "lastValue",
    }
  ]);
});

test("should work with array of simple types", () => {
  const input = [
    "a",
    "b",
    "c",
  ]

  const output = retrieveJson(input, "version1");

  expect(output).toStrictEqual([
    "a",
    "b",
    "c",
  ]);
});

test("complex example", () => {
  const input = {
    key: {
      nextKey__version1: "value1",
      nextKey__version2: "value2",
    },
    items: [
      {
        __include: "version1",
        key: "value",
        otherKey__version1: "recursiveValue",
        otherKey__version2: "notPossible",
      },
      {
        __include: "version2",
        otherKey: "otherValue",
      },
      {
        justKey: "justValue",
        otherKey__version1: [
          {
            key: "value"
          },
          {
            __include: "version1",
            key: "value1"
          },
          {
            __include: "version2",
            key: "value2"
          },
        ],
        otherKey__version2: "notPossible",
      },
      {
        __include: ["version1", "version2"],
        key: "matchOneOfTwoVersions"
      },
      {
        __include: ["version2", "version3"],
        key: "dontMatchOneOfTwoVersions"
      }
    ],
  }

  const output = retrieveJson(input, "version1");

  expect(output).toStrictEqual({
    key: {
      nextKey: "value1",
    },
    items: [
      {
        key: "value",
        otherKey: "recursiveValue",
      },
      {
        justKey: "justValue",
        otherKey: [
          {
            key: "value"
          },
          {
            key: "value1"
          },
        ]
      },
      {
        key: "matchOneOfTwoVersions"
      },
    ]
  });
});