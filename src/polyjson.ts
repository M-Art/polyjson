interface PolyjsonOptions {
  key: string,
}

const defaultOptions: PolyjsonOptions = {
  key: "__",
}

export function retrieveJson(json: object, version: string) {
  const jsonCopy = JSON.parse(JSON.stringify(json));

  return parse(defaultOptions, version, jsonCopy);
}

function parse(options: PolyjsonOptions, version: string, input: any): any {
  if (isArrayObject(input)) {
    return parseArrayObject(options, version, input);
  } else if (isObject(input)) {
    return parseObject(options, version, input);
  } else {
    return input;
  }
}

function parseArrayObject(options: PolyjsonOptions, version: string, input: any[]) {
  const output = [];

  for (const item of input) {
    if (isObject(item) && !isArrayObject(item)) {
      const itemContainsIncludeProperty = (options.key + "include") in item;

      if (itemContainsIncludeProperty) {
        const includeValue = item[options.key + "include"];

        let include = false;

        if (isArrayObject(includeValue)) {
          include = includeValue.indexOf(version) !== -1;
        } else {
          include = includeValue === version;
        }

        if (include) {
          delete item[options.key + "include"];
          const parsedItem = parse(options, version, item);
          output.push(parsedItem);
        }
      } else {
        const parsedItem = parse(options, version, item);
        output.push(parsedItem);
      }
    } else {
      const parsedItem = parse(options, version, item);
      output.push(parsedItem);
    }
  }

  return output;
}

function parseObject(options: PolyjsonOptions, version: string, input: any) {
  const output: any = {};

  for (let key in input) {
    const keyContainsVersion = key.indexOf(options.key) != -1;

    if (keyContainsVersion) {
      const [keyName, keyVersion] = key.split(options.key);

      if (keyVersion === version) {
        output[keyName] = parse(options, version, input[key]);
      }
    } else {
      output[key] = parse(options, version, input[key]);
    }
  }

  return output;
}

function isObject(obj: any): boolean {
  return typeof(obj) == 'object';
}

function isArrayObject(obj: any): boolean {
  return isObject(obj) && obj instanceof Array;
}